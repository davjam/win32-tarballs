# GHC's Mingw toolchain

This repository is used to build the mingw toolchain used by GHC on Windows.
This is necessary because `gcc` and `binutils` are affected by a variety of
bugs to do with long-path handling on Windows due to their use of POSIX
functions implemented by `msvcrt`.

To mitigate these bugs, we provide a patched toolchain (built
using the `ghc-jailbreak` utility included in this repository)
which overrides various `msvcrt` implementations with
alternatives written in terms of native Win32 functions.
