#!/usr/bin/env bash

set -ex

ARCH="$1"

case $ARCH in
  x86_64) mingw=mingw64 ;;
  i686) mingw=mingw32 ;;
  *) echo "Unknown architecture $ARCH"; exit 1 ;;
esac

rm -Rf tmp
mkdir -p tmp
cd tmp

for f in $(cat ../tarballs/$ARCH/MANIFEST); do
  tar -xf ../tarballs/$ARCH/$f
done

cat >hello.c <<EOF
#include <stdio.h>
int main() {
  printf("Hello world!");
  return 0;
}
EOF

CC=$mingw/bin/gcc.exe
export PATH=`pwd`/$mingw/bin:$PATH
$CC --version
$CC hello.c -o hello
./hello


$CC --print-search-dirs | grep -v -q '[ABDEFGH]:/' || \
    ( echo "found upstream packaging error: non-C: paths in gcc search path (see ghc#18774)"; exit 1 )

cd ..
rm -Rf tmp
