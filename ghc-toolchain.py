#!/usr/bin/env python3

from typing import List, Dict, NewType
import requests
from pathlib import Path
import tempfile
import subprocess
import hashlib
import shutil

Arch = NewType('Arch', str)
Url = str

#UPSTREAM_URL = 'http://repo.msys2.org/mingw'
UPSTREAM_URL = 'https://mirror.selfnet.de/msys2/mingw'

CHECK_SIGNATURES = False
JAILBREAK = Path('ghc-jailbreak')

# Known architectures (upstream names)
ARCHS = ['x86_64', 'i686', 'sources'] # type: List[Arch]

def package(name: str, archs: List[Arch] = ARCHS, ext: str = 'zst') -> Dict[Arch, Url]:
    # N.B. ext is to accomodate transition from .xz to .zst archives.
    URLS = {
        'x86_64':  f'/x86_64/mingw-w64-x86_64-{name}-any.pkg.tar.{ext}',
        'i686':  f'/i686/mingw-w64-i686-{name}-any.pkg.tar.{ext}',
        'sources':  f'/sources/mingw-w64-{name}.src.tar.gz',
    }
    return { arch: URLS[arch] for arch in archs }

# Version numbers
crt_ver = '9.0.0.6316.acdc7adc9'
winpthreads_ver = '8.0.0.5904.1278c953'
gcc_ver = '10.2.0'

# type: List[Dict[Arch, Url]]
packages = [
    package(f'crt-git-{crt_ver}-1'),
    package(f'headers-git-{crt_ver}-1'),
    package(f'winpthreads-git-{winpthreads_ver}-1'),
    package(f'libwinpthread-git-{winpthreads_ver}-1', archs=['i686', 'x86_64']),
    package('zlib-1.2.11-9', ext='zst'),
    package('isl-0.22.1-1', ext='xz'),
    package('mpfr-4.1.0-3', ext='zst'),
    package('gmp-6.2.0-1', ext='xz'),
    package('zstd-1.4.5-1'),
    package('binutils-2.34-2'),
    package('libidn2-2.3.0-1', ext='xz'),
    package(f'gcc-{gcc_ver}-3'),
    package('mpc-1.2.1-1', ext='zst'),
    package('windows-default-manifest-6.4-3', ext='xz'),
    package(f'gcc-libs-{gcc_ver}-3', archs=['i686', 'x86_64']),
]

def download(url: str, dest: Path):
    print(f'Fetching {url} => {dest}...')
    r = requests.get(url, stream=True)
    r.raise_for_status()
    out = dest.open('wb')
    for chunk in r.iter_content(chunk_size=128):
        out.write(chunk)

def download_tarballs(dest_dir: Path) -> Dict[Arch, List[Path]]:
    tarballs = { arch: [] for arch in ARCHS }
    for pkg in packages:
        for arch in ARCHS:
            if arch in pkg:
                (dest_dir / arch).mkdir(parents=True, exist_ok=True)
                url = f'{UPSTREAM_URL}{pkg[arch]}'
                dest = dest_dir / arch / Path(pkg[arch]).name
                dest_sig = dest.with_name(dest.name + '.sig')
                if not dest.exists():
                    download(url, dest)

                if CHECK_SIGNATURES:
                    download(url + '.sig', dest_sig)
                    subprocess.check_call(['gpgv', '-v',  '--keyring', './mingw-key.gpg', dest_sig, dest])
                tarballs[arch].append(dest)

    return tarballs

def patch_executable(arch: Arch, exe: Path) -> None:
    patcher_dir = JAILBREAK / arch
    patcher = patcher_dir / 'iat-patcher.exe'
    print(f'Patching {exe} with {patcher}...')
    subprocess.check_call([patcher, 'install', exe])

    for dll in patcher_dir.glob('*.dll'):
        dest = exe.parent / dll.name
        if not dest.is_file():
            shutil.copyfile(dll, dest)

def needs_patching(tarball: Path) -> bool:
    return 'binutils' in tarball.name or 'gcc' in tarball.name

def patch_tarball(arch: Arch, tarball: Path) -> Path:
    if not needs_patching(tarball):
        return tarball

    dest = tarball.with_name(tarball.name.replace('-any', '-phyx'))
    if not dest.exists():
        print(f'Patching tarball {tarball} for {arch}...')
        # We always recompress to xz
        dest = dest.with_suffix('.xz')
        with tempfile.TemporaryDirectory() as tmp:
            subprocess.check_call(['tar', '-xf', tarball, '-C', tmp])
            for f in Path(tmp).glob('**/*.exe'):
                patch_executable(arch, f)

            subprocess.check_call(['tar', '-cJf', dest, '-C', tmp, '.'])

    return dest

def hash_file(path: Path) -> Path:
    h = hashlib.new('sha256')
    h.update(path.read_bytes())
    return h.hexdigest()

def main() -> None:
    dest = Path('tarballs')
    tarballs = download_tarballs(dest)

    for arch, files in tarballs.items():
        d = dest / arch
        patched_files = []
        for f in files:
            tarball = patch_tarball(arch, f)
            hash = hash_file(tarball)
            patched_files.append(tarball.relative_to(d))

        subprocess.run(['sha256sum'] + [f.relative_to(d) for f in d.iterdir()],
                       stdout=open(d / 'SHA256SUMS', 'wb'),
                       cwd=d,
                       check=True)
        (d / "MANIFEST").write_text('\n'.join(str(f) for f in patched_files))

    # Preserve commit hash
    commit = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    (dest / "COMMIT").write_bytes(commit)

    shutil.copyfile('ghc-jailbreak.tar.gz', dest / 'ghc-jailbreak.tar.gz')

if __name__ == '__main__':
    main()
